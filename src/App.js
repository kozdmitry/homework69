import React from 'react';
import Layout from "./component/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import ShowPage from "./container/showPage/showPage";
import Home from "./container/Home/Home";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/shows/:id" component={ShowPage}/>
            </Switch>
        </Layout>
    );
};

export default App;