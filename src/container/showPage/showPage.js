import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchInfoSerial} from "../../store/action";
import {Container, Grid, Typography} from "@material-ui/core";
import "./showPage.css";
import Input from "../../component/Input/Input";

const ShowPage = (props) => {

    const dispatch = useDispatch();
    const info = useSelector((state) => state.info);

    useEffect(() => {
        dispatch(fetchInfoSerial(props.match.params.id))
    }, [props.match.params.id, dispatch]);


    return (
        <>
            <div className="info">
                <Container>
                    <Grid container spacing={2}>
                        <Grid item>
                            <Typography>
                                <Input history={props.history}/>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography>
                                {/*<img src={info.image.medium}/>*/}
                            </Typography>
                            <Typography variant="h4">
                                {info.name}
                            </Typography>
                            <Grid item>
                                <Typography>
                                    {info.summary}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        </>

    );
};

export default ShowPage;