import {
    FETCH_INFO_FAILURE,
    FETCH_INFO_REQUEST,
    FETCH_INFO_SUCCESS,
    FETCH_SERIAL_FAILURE,
    FETCH_SERIAL_REQUEST,
    FETCH_SERIAL_SUCCESS, GET_FETCH_INFO_SERIAL
} from "./action";

const initialState = {
    serials: [],
    info: [],
    loading: false,
    error: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SERIAL_REQUEST:
            return {...state, loading: true};
        case FETCH_SERIAL_SUCCESS:
            return {...state, loading: false, serials: action.serials};
        case FETCH_SERIAL_FAILURE:
            return {...state, loading: false, error: false};
        case FETCH_INFO_REQUEST:
            return {...state, loading: true};
        case FETCH_INFO_SUCCESS:
            return {...state, info: action.info, loading: false};
        case FETCH_INFO_FAILURE:
            return {...state, loading: false, error: false};
        case GET_FETCH_INFO_SERIAL:
            return {...state, info: action.info};
        default:
            return state;
    }
};

export default reducer;