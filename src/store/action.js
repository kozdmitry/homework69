import axios from 'axios';

export const FETCH_SERIAL_REQUEST = 'FETCH_SERIAL_REQUEST';
export const FETCH_SERIAL_SUCCESS = 'FETCH_SERIAL_SUCCESS';
export const FETCH_SERIAL_FAILURE = 'FETCH_SERIAL_FAILURE';
export const GET_FETCH_INFO_SERIAL = 'GET_FETCH_INFO_SERIAL';


export const fetchSerialRequest = () => ({type: FETCH_SERIAL_REQUEST});
export const fetchSerialSuccess = serials => ({type: FETCH_SERIAL_SUCCESS, serials});
export const fetchSerialFailure = () => ({type: FETCH_SERIAL_FAILURE});
export const getFetchInfoSerial = info => ({type: GET_FETCH_INFO_SERIAL, info});


export const FETCH_INFO_REQUEST = 'FETCH_INFO_REQUEST';
export const FETCH_INFO_SUCCESS = 'FETCH_INFO_SUCCESS';
export const FETCH_INFO_FAILURE = 'FETCH_INFO_FAILURE';

export const fetchInfoRequest = () => ({type: FETCH_INFO_REQUEST});
export const fetchInfoSuccess = info => ({type: FETCH_INFO_SUCCESS, info});
export const fetchInfoFailure = () => ({type: FETCH_INFO_FAILURE});

export const fetchSerial = (text) => {
    return async (dispatch) => {
        dispatch(fetchSerialRequest());
        try {
            const response = await axios.get('http://api.tvmaze.com/search/shows?q=' + text);
            console.log(response.data);
            dispatch (fetchSerialSuccess(response.data));
        } catch (e) {
            dispatch(fetchSerialFailure());
        }
    }
};

export const fetchInfoSerial = (id) => {
    return async  dispatch => {
        dispatch(fetchInfoRequest());
        try {
            const response = await axios.get('http://api.tvmaze.com/shows/' + id);
            dispatch (getFetchInfoSerial(response.data));
        } catch (e) {
            dispatch(fetchInfoFailure());
        }
    }
};

