import React, {useEffect, useState} from 'react';
import {Grid, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchSerial} from "../../store/action";
import {NavLink} from "react-router-dom";

const Input = (props) => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const [text, setText] = useState('');

    useEffect(() => {
        if (text.length > 1) {
        }  dispatch(fetchSerial(text));
    }, [text, dispatch]);

    const goToPage = (id) => {
        setText('');
        props.history.push({
            pathname: `/shows/${id}`
        });
    };

    const changeInfo = async (e) => {
        const value = e.target.value;
        setText(value);
    };

    return (
        <>
            <Typography>

            </Typography>
            <Grid item xs={12} spacing={2} className="input">
                <h1>Search for TV show!</h1>
            </Grid>
            <Grid item xs={12} spacing={2}>
                <TextField
                    type="text"
                    placeholder="Title"
                    name="title"
                    variant="outlined"
                    autoComplete="off"
                    onChange={(e) => changeInfo(e)}
                    value={text}
                />
            </Grid>
            <Grid>
                {state.serials.map((serial, index) => {
                    return (
                        <div key={index}>
                            <p onClick={() => goToPage(serial.show.id)}>{serial.show.name}</p>
                        </div>
                    )
                })}
            </Grid>
        </>
    );
};

export default Input;