import React from 'react';
import "./Layout.css";
import {AppBar, CssBaseline, Grid, Toolbar, Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const Layout = (props) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography variant="h6" noWrap>
                                <NavLink className="btn" to="/">TV Shows</NavLink>
                            </Typography>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar>
            </Toolbar>
            <main>
                {props.children}
            </main>
        </>
    );
};

export default Layout;